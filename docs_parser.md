1 ý tưởng để có thể  "Cho phép cấu hình đường dẫn file users.json ra đâu tùy ý"  là gán đường dẫn file đó vào 1 tham số , truyền giá trị mong muốn cho nó qua việc chạy file python kết hợp với tham số và giá trị theo cấu trúc: `python filename.py "value_arg1" "value_arg2"`   
Module Argparse cho phép bạn thực hiện mục đích này.   
Để sử dụng Molule Argparse chỉ cần import thư viện thư viện `argparse` vào trong code và khởi tạo object `ArgumentParser` -  chứa tất cả thông tin cần thiết để phân tích dòng lệnh thành các kiểu dữ liệu Python.  
```
import argparse
parser = argparse.ArgumentParser(description='path file')
``` 
Để thêm 1 thông tin về 1 đối số cho ArgumentParser, ta sử dụng phương thức `add_argument()`  
```
parser.add_argument('path', help='path file', nargs='?', default='data/users.json') 
``` 
- `'path'`: tên của đối số khi truyền vào dòng lệnh, ngoài ra có thể đặt là 1 list các string ( như `-p`,`--path`) 
- `help='path file'`: Mô tả ngắn gọn về đối số đang khai báo  
- `nargs='?', default='data/users.json'`: `nargs` chỉ số lượng đối số bạn cần  ( default=1), khi nhập `?` thì nếu dòng lệnh không nhập tham số nào thêm, nó sẽ lấy giá trị từ `default` (ở đây default='data/users.json') 
Sau khi thêm xong các đối số, để conver các tham số nhận được thành một object và gán nó thành một thuộc tính của một namespace, ta sử dụng phương thức `parse_args()` 
```
data = parser.parse_args() 
fname = data.path
```  
- `fname = data.path` để gán giá trị nhận được từ đối số  `path` nhập từ dòng lệnh vào biến `fname` 

full code: 
``` 
import argparse
parser = argparse.ArgumentParser(description='path file')
parser.add_argument('path', help='path file', nargs='?', default='data/users.json')  
data = parser.parse_args()                                                          
fname = data.path
``` 
Test: 
- Muốn config đường dẫn đến file là test/users.json : `$python app.py "test/users.json"`
- Muốn dùng đường dẫn đến file là mặc định (data/users.json) : `$python app.py` 

